import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {
    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }



    public static final String COURSES_BUTTON = "//*[@id='course']";
    public static final String GENERAL_TESTING_COURSES_BUTTON = "//*[@id='courseMenuNav']/div/div[1]/ul/li[2]/a/span";
    public static final String MANUAL_COURSES_BUTTON = "//*[@id='courseMenuNav']/div/div[3]/div[1]/div/div[13]/section/a[1]/div/h3";
    public static final String ABOUT_MANUAL_COURSES = "/html/body/section[1]/div/article/div[1]/div/strong";
    public static final String ABOUT_KYIV_MANUAL_COURSES = "/html/body/section[1]/div/article/div[1]/div";
    public static final String SELECT_CITIES_MANUAL_COURSES = "/html/body/section[1]/div/article/div[1]/div/div/button";
    public static final String SELECT_CITY_KYIV_MANUAL_COURSES = "//*[@id='menuCityIntro']/li[1]/a";
    public static final String GENERAL_DEVELOPING_COURSES_BUTTON = "//*[@id='courseMenuNav']/div/div[1]/ul/li[1]/a";
    public static final String FRONT_END_DEVELOPING_COURSES_BUTTON = "//*[@id='programming']/li[1]/button";
    public static final String FRONT_END_BASIC_COURSES_BUTTON = "//*[@id='courseMenuNav']/div/div[3]/div[1]/div/div[1]/section[1]/a[1]/div";
    public static final String BASIC_BADGE = "/html/body/section[1]/div/article/div[1]/div/strong";

    public static final By COURSES_BUTTON_by = By.xpath( "//*[@id='course']");
    public static final By GENERAL_TESTING_COURSES_BUTTON_by = By.xpath("//*[@id='courseMenuNav']/div/div[1]/ul/li[2]/a/span");
    public static final By MANUAL_COURSES_BUTTON_by = By.xpath("//*[@id='courseMenuNav']/div/div[3]/div[1]/div/div[13]/section/a[1]/div/h3");
    public static final By ABOUT_MANUAL_COURSES_by = By.xpath("/html/body/section[1]/div/article/div[1]/div/strong");
    public static final By ABOUT_KYIV_MANUAL_COURSES_by = By.xpath("/html/body/section[1]/div/article/div[1]/div");
    public static final By SELECT_CITIES_MANUAL_COURSES_by = By.xpath("/html/body/section[1]/div/article/div[1]/div/div/button");
    public static final By SELECT_CITY_KYIV_MANUAL_COURSES_by = By.xpath("//*[@id='menuCityIntro']/li[1]/a");
    public static final By GENERAL_DEVELOPING_COURSES_BUTTON_by = By.xpath("//*[@id='courseMenuNav']/div/div[1]/ul/li[1]/a");
    public static final By FRONT_END_DEVELOPING_COURSES_BUTTON_by = By.xpath("//*[@id='programming']/li[1]/button");
    public static final By FRONT_END_BASIC_COURSES_BUTTON_by = By.xpath("//*[@id='courseMenuNav']/div/div[3]/div[1]/div/div[1]/section[1]/a[1]/div");
    public static final By BASIC_BADGE_by = By.xpath("/html/body/section[1]/div/article/div[1]/div/strong");

    @FindBy(xpath = COURSES_BUTTON)
    public WebElement courseButton;

    public WebElement getCourseButton() {
        return courseButton;
    }
    @FindBy(xpath = GENERAL_TESTING_COURSES_BUTTON)
    public WebElement generalTestingCoursesButton;

    public WebElement getGeneralTestingCoursesButton() {
        return generalTestingCoursesButton;
    }

    @FindBy(xpath = MANUAL_COURSES_BUTTON)
    public WebElement manualCoursesButton;

    public WebElement getManualCoursesButton() {
        return manualCoursesButton;
    }

    @FindBy(xpath = ABOUT_MANUAL_COURSES)
    public WebElement aboutManualCourses;

    public WebElement getAboutManualCourses() {
        return aboutManualCourses;
    }

    @FindBy(xpath = ABOUT_KYIV_MANUAL_COURSES)
    public WebElement aboutKyivManualCourses;

    public WebElement getAboutKyivManualCourses() {
        return aboutKyivManualCourses;
    }

    @FindBy(xpath = SELECT_CITIES_MANUAL_COURSES)
    public WebElement selectCitiesManualCourses;

    public WebElement getSelectCitiesManualCourses() {
        return selectCitiesManualCourses;
    }

    @FindBy(xpath = SELECT_CITY_KYIV_MANUAL_COURSES)
    public WebElement selectCityKyivManualCouses;

    public WebElement getSelectCityKyivManualCouses() {
        return selectCityKyivManualCouses;
    }

    @FindBy(xpath = GENERAL_DEVELOPING_COURSES_BUTTON)
    public WebElement generalDevelopingCoursesButton;

    public WebElement getGeneralDevelopingCoursesButton() {
        return generalDevelopingCoursesButton;
    }
    @FindBy(xpath = FRONT_END_DEVELOPING_COURSES_BUTTON)
    public WebElement frontEndDevelopingCoursesButton;

    public WebElement getFrontEndDevelopingCoursesButton() {
        return frontEndDevelopingCoursesButton;
    }
    @FindBy(xpath = FRONT_END_BASIC_COURSES_BUTTON)
    public WebElement frontEndBasicCoursesButton;

    public WebElement getFrontEndBasicCoursesButton() {
        return frontEndBasicCoursesButton;
    }
    @FindBy(xpath = BASIC_BADGE)
    public WebElement basicBadge;

    public WebElement getBasicBadge() {
        return basicBadge;
    }
}

