import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class WebDriverHW {
    @Test
    public void firstSeleniumSuperTest() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver",
                "C:\\Olga\\Hillel\\Kateryna\\logvynenko_hworks\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        HomePage homePage = new HomePage(driver);
        driver.get("https://kharkiv.ithillel.ua/");
        Thread.sleep(1000);
        homePage.getCourseButton().click();
        homePage.getGeneralDevelopingCoursesButton().click();
        homePage.getFrontEndDevelopingCoursesButton().click();
        homePage.getFrontEndBasicCoursesButton().click();
        assertTrue(homePage.getBasicBadge().isDisplayed(), "BASIC");
        driver.close();
    }

    @Test
    public void secondSeleniumSuperTest() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver",
                "C:\\Olga\\Hillel\\Kateryna\\logvynenko_hworks\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        HomePage homePage = new HomePage(driver);
        driver.get("https://kharkiv.ithillel.ua/");
        Thread.sleep(1000);
        homePage.getCourseButton().click();
        homePage.getGeneralTestingCoursesButton().click();
        homePage.getManualCoursesButton().click();
        homePage.getAboutManualCourses();
        assertTrue(homePage.getAboutManualCourses().isDisplayed());
    }

    @Test
    public void thirdSeleniumSuperTest() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver",
                "C:\\Olga\\Hillel\\Kateryna\\logvynenko_hworks\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        HomePage homePage = new HomePage(driver);
        driver.get("https://kharkiv.ithillel.ua/");
        Thread.sleep(1000);
        homePage.getCourseButton().click();
        homePage.getGeneralTestingCoursesButton().click();
        homePage.getManualCoursesButton().click();
        homePage.getSelectCitiesManualCourses().click();
        homePage.getSelectCityKyivManualCouses().click();
        assertTrue(homePage.getAboutKyivManualCourses().isDisplayed());
        driver.close();
    }
}
